import sys
import os
import csv
import yaml
import json
import re
sys.path.append('./bobtail')
sys.path.append('./bobtail/src/')
sys.path.append('./bobtail/src/bobtail')

from rasa_nlu.converters import load_data
from rasa_nlu.config import RasaNLUConfig
from rasa_nlu.model import Trainer

from bobtail.node import Node
from bobtail.node_soup import NodeSoup
from bobtail.interpreter import Interpreter


def train():
    training_data = load_data('data/training/all_examples.json')
    trainer = Trainer(RasaNLUConfig("config.json"))
    trainer.train(training_data)
    model_directory = trainer.persist(
        './data/models/')  # Returns the directory the model is stored in


def import_examples():
    all_examples = []
    example_dir = './data/examples/'

    for example_file in os.listdir(example_dir):
        if example_file[-4:] == '.csv':
            print("Importing {0}".format(example_file))
            examples = import_example_file(
                os.path.join(example_dir, example_file))
            all_examples = all_examples + examples

    doc = {"rasa_nlu_data": {"common_examples": all_examples}}

    with open("./data/training/all_examples.json", 'w') as outfile:
        json.dump(doc, outfile, indent=4)


def import_example_file(example_file):
    examples = []
    entities = None

    with open('./data/entities.yml') as f:
        entities = yaml.safe_load(f)

    with open(example_file, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            example = {"text": row[1], "intent": row[0], "entities": []}
            with_entities = expand_example_with_entities(example, entities)
            examples.append(example)

    return examples


def expand_example_with_entities(example, entities):
    for name, values in entities.items():
        for value in values:
            matcher = r'\b({0})\b'.format(value)
            match = re.search(matcher, example["text"], flags=re.IGNORECASE)
            if match:
                example["entities"].append({
                    "entity": name,
                    "value": value,
                    "start": match.start(),
                    "end": match.end()
                })

    return example


def say(text):
    print("\U0001F419 {0} ".format(text))


def listen():
    latest_model = get_latest_model()
    print("Using model {0}".format(latest_model))

    interpreter = Interpreter(None, "config.json",
                              "./data/models/default/{0}".format(latest_model))
    soup = create_node_soup()
    intro = True

    while True:
        say(soup.introduction) if intro else say(
            "Anything else I can do for you?")
        intro = False

        response = input("> ")
        interpreted_response = interpreter.process(response)
        ask(soup, interpreter, interpreted_response['intent'])


def ask(soup, interpreter, intent):
    nodes = soup.find_nodes(intent)
    if len(nodes) == 0:
        say("Sorry, I don't understand what you mean by that")
        return

    node = nodes[0]

    def do_interrupt(interrupt):
        if interrupt["type"] == "ask":
            say(interrupt["data"])
            answer = input("> ")
            interpreted_answer = interpreter.process(answer)
            return interpreted_answer["intent"]
        elif interrupt["type"] == "message":
            say(interrupt["data"])
        elif interrupt["type"] == "quit":
            exit()

    gen = node.process()
    val = None
    try:
        while True:
            val = do_interrupt(gen.send(val))
    except StopIteration:
        pass


def create_node_soup():
    soup = NodeSoup("Hello. What can I do for you today?")
    soup.load('./data/definitions.yml')
    return soup


def get_latest_model():
    return sorted(os.listdir("./data/models/default"))[-1]


def test_intents():
    latest_model = get_latest_model()
    print("Using model {0}".format(latest_model))

    interpreter = Interpreter(None, "config.json",
                              "./data/models/default/{0}".format(latest_model))

    while True:
        text = input("\U0001F419 > ")
        result = interpreter.process(text)
        print("{0} - {1}".format(result["intent"], result["confidence"]))


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == 'train':
        train()
    elif len(sys.argv) > 1 and sys.argv[1] == 'import':
        import_examples()
    elif len(sys.argv) > 1 and sys.argv[1] == 'intents':
        test_intents()
    else:
        listen()
