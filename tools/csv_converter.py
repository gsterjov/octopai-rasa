import csv
import json
import sys

with open(sys.argv[1], 'r') as csvfile:
    reader = csv.reader(csvfile)
    examples = []
    for row in reader:
        example = {"text": row[0], "intent": row[1], "entities": []}
        examples.append(example)

    out = {"rasa_nlu_data": {"common_examples": examples}}

    print(json.dumps(out, indent=4))
