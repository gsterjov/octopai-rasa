from rasa_nlu.converters import load_data
from rasa_nlu.config import RasaNLUConfig
from rasa_nlu.model import Trainer

from importer import Importer
from exporter import Exporter


if __name__ == '__main__':
  importer = Importer('./sample.json')
  exporter = Exporter('./rasa-sample.json')
  examples = importer.examples()
  exporter.export(examples)

# training_data = load_data('data/training/common.json')
# trainer = Trainer(RasaNLUConfig("config_spacy.json"))
# trainer.train(training_data)
# model_directory = trainer.persist('./models/')  # Returns the directory the model is stored in
